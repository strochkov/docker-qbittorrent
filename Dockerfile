FROM gliderlabs/alpine:latest

# Install required packages
RUN apk add --no-cache \
      boost-system \
      boost-thread \
      ca-certificates \
      qt5-qtbase

COPY main.patch /

RUN set -x \
    # Install build dependencies
 && apk add --no-cache -t deps \
      boost-dev \
      curl \
      cmake \
      g++ \
      make \
      qt5-qttools-dev \
    \
    # Build lib rasterbar from source code (required by qBittorrent)
 && LIBTORRENT_RASTERBAR_URL=$(curl -L http://www.qbittorrent.org/download.php | grep -Eo 'https?://[^"]*libtorrent[^"]*\.tar\.gz[^"]*' | head -n1) \
 && curl -L $LIBTORRENT_RASTERBAR_URL | tar xzC /tmp \
 && cd /tmp/libtorrent-rasterbar* \
 && mkdir build \
 && cd build \
 && cmake .. \
 && make install \
    \
    # Build qBittorrent from source code
 && QBITTORRENT_URL=$(curl -L http://www.qbittorrent.org/download.php | grep -Eo 'https?://[^"]*qbittorrent[^"]*\.tar\.xz[^"]*' | head -n1) \
 && curl -L $QBITTORRENT_URL | tar xJC /tmp \
 && cd /tmp/qbittorrent* \
 && ln -s /usr/bin/lrelease /usr/bin/lrelease-qt4 \
 && PKG_CONFIG_PATH=/usr/local/lib/pkgconfig ./configure --disable-gui \
    # Patch: Disable stack trace because it requires libexecline-dev which isn't available on Alpine 3.4.
 && cd src/app \
 && patch -i /main.patch \
 && rm /main.patch \
 && cd ../.. \
 && make install \
    \
    # Clean-up
 && cd / \
 && apk del --purge deps \
 && rm -rf /tmp/*

# Create symbolic links to simplify mounting
RUN mkdir -p /root/.config/qBittorrent \
 && mkdir -p /root/.local/share/data/qBittorrent \
 && mkdir /downloads \
 && chmod go+rw -R /root /downloads \
 && ln -s /root/.config/qBittorrent /config \
 && ln -s /root/.local/share/data/qBittorrent /torrents


 # Default configuration file.
COPY qBittorrent.conf /default/qBittorrent.conf
COPY entrypoint.sh /

VOLUME ["/config", "/torrents", "/downloads"]

EXPOSE 8080 6881

ENTRYPOINT ["/entrypoint.sh"]
CMD ["qbittorrent-nox"]